## References

* [Building my FreeBSD-based home router](https://kamila.is/learning/building-my-home-router/)
* [Forwarding performance lab of a PC Engines APU 2](https://bsdrp.net/documentation/examples/forwarding_performance_lab_of_a_pc_engines_apu2)
* [BSD Router Project: Open Source Router Distribution](https://bsdrp.net/BSDRP)

