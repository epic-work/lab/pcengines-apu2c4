# Install Debian 10 on PCEngines box


```sh
$ curl -L -O https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.1.0-amd64-netinst.iso
```

Check shasums
```
$ shasum -a 256 debian-10.1.0-amd64-netinst.iso

7915fdb77a0c2623b4481fc5f0a8052330defe1cde1e0834ff233818dc6f301e  debian-10.1.0-amd64-netinst.iso
```


Before we write to the USB we first need to identify which /dev/{device-name} we are referring to. To do this run:

```
$ hdiutil convert -format UDRW -o debian-10.1.0-amd64-netinst.iso debian-10.1.0-amd64-netinst.iso
```

```
$ diskutil list
```
Next make sure to unmount the disk so dd can do some magic on it:
```
$ diskutil unmountDisk /dev/disk2

Unmount of all volumes on disk2 was successful
```

```
$ sudo dd if=debian-10.1.0-amd64-netinst.iso.dmg of=/dev/rdisk2 bs=1m conv=sync
```

```
              �┌�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�┐
              �│  Debian GNU/Linux installer menu (BIOS mode)  �│
              �├�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�┤��─��
              �│ Graphical install                              �│
              �│ Install                                        �│
              �│ Advanced options                             > �│
              �│ Accessible dark contrast installer menu      > �│
              �│ Help                                          �│
              �│ Install with speech synthesis                  �│
              �│                                                �│
              �│                                                �│
              �│                                                �│
              �│                                                �│
              �│                                                �│
              �│                                               �│
              �└�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�─�┘
               Press ENTER to boot or TAB to edit a menu entry
```

Press `H`, enter the following parameters and press Enter.

```
install vga=off console=ttyS0,115200n8

Press <ENTER> to see video modes available, <SPACE> to continue, or wait 30 sec
```

Press `<ENTER>`
```
Mode: Resolution:  Type:
0 F00   80x25      CGA/MDA/HGC
Enter a video mode or "scan" to scan for additional modes
```

Press `0 <ENTER>`

... nothing happens


vga=off console=ttyS0,115200n8 initrd=/install.amd/initrd.gz --- quiet console=ttyS0,115200n8


/install.amd/vmlinuz loadmodules=usbserial vga=off console=ttyUSB0,115200u8 initrd=/install.amd/initrd.gz --- quiet console=ttyUSB0,115200u8




During the boot, I escaped to loader prompt (3) and added:
Code:
```
set boot_multicons="YES"
set boot_serial="YES"
set comconsole_speed="115200"
set console="comconsole,vidconsole"
boot
```


```
set comconsole_speed="115200"
set console="comconsole"
set vfs.mountroot.timeout="10"
boot
```


## References

* [teklager.se - Installing Debian over serial console on APU board](https://teklager.se/en/knowledge-base/installing-debian-over-serial-console-apu-board/)
