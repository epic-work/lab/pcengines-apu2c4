PCEngines apu2
coreboot build 20170228
4080 MB ECC DRAM

SeaBIOS (version rel-1.10.0.1)


```
Memtest86+ 5.01 coreboot 001| AMD GX-412TC SOC
CLK: 998.2MHz  (X64 Mode)   | Pass  0%
L1 Cache:   32K  15123 MB/s | Test 23% ########
L2 Cache: 2048K   5424 MB/s | Test #3  [Moving inversions, 1s & 0s Parallel]
L3 Cache:  None             | Testing: 1024K - 2048M   2047M of 4079M
Memory  : 4079M   1523 MB/s | Pattern:   00000000           | Time:   0:00:17
------------------------------------------------------------------------------
Core#: 0 (SMP: Disabled)  |  CPU Temp  | RAM: 666 MHz (DDR3-1333) - BCLK: 100
State: - Running...       |    46 C    | Timings: CAS 9-9-10-24 @ 64-bit Mode
Cores:  1 Active /  1 Total (Run: All) | Pass:       0        Errors:      0
------------------------------------------------------------------------------












                                PC Engines APU2
(ESC)exit  (c)configuration  (SP)scroll_lock  (CR)scroll_unlock (l)refresh
```

