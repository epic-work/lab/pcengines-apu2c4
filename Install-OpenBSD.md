### Install OpenBSD

```

$ curl -L -O https://cdn.openbsd.org/pub/OpenBSD/6.5/amd64/install65.fs
$ curl -L -O https://cdn.openbsd.org/pub/OpenBSD/6.5/amd64/SHA256

shasum -a 256 install65.fs # ed420c77ac82a768e1116c3b72f0811cb03091de90990460dd39ade9db06c3cf
# Matches the install65.fs line item @SHA256
```

```
$ diskutil unmountDisk /dev/diskN
```

Copy the install filesystem to a bootable USB disk
```
$ sudo dd if=install65.fs of=/dev/rdisk2 bs=1m conv=sync
```

Start the PCEngines board
```
$ screen /dev/tty.SLAB_USBtoUART 115200
```

```
coreboot build 20190905
BIOS version v4.9.0.5
4080 MB ECC DRAM
SeaBIOS (version rel-1.12.1.1-0-g55d345f)

Press F10 key now for boot menu

Select boot device:

1. USB MSC Drive Samsung Flash Drive 1100
2. AHCI/0: Samsung SSD 860 EVO mSATA 250GB ATA-11 Hard-Disk (232 GiBytes)
3. Payload [setup]
4. Payload [memtest]
```

Set the serial console parameters:

**NOTE: Do NOT copy-paste the input (e.g. `stty com0 115200`) to the `screen`. This results in unexpected boot behavior.**
```
Booting from Hard Disk...
Using drive 0, partition 3.
Loading......
probing: pc0 com0 com1 com2 com3 mem[639K 3325M 752M a20=on]
disk: hd0+ hd1+*
>> OpenBSD/amd64 BOOT 3.43
boot> stty com0 115200
boot> set tty com0
switching console to com>> OpenBSD/amd64 BOOT 3.34
boot> [Press Enter]
```

Select the Install option:
```
Welcome to the OpenBSD/amd64 6.4 installation program.
(I)nstall, (U)pgrade, (A)utoinstall or (S)hell? I
```

Select terminal type
```
Terminal type? [vt220]
```

When presented with a list of network interfaces, em0 is the Ethernet port closest to the serial port:
```
Available network interfaces are: em0 em1 em2 em3 vlan0.
```

```
Available disks are: sd0 sd1.
Which disk is the root disk? ('?' for details) [sd0] sd0
Disk: sd0       Usable LBA: 40 to 488397127 [488397168 Sectors]
   #: type                                 [       start:         size ]
------------------------------------------------------------------------
   0: 83bd6b9d-7f41-11dc-be0b-001560b84f0f [          40:         1024 ]
   1: 516e7cb5-6ecf-11d6-8ff8-00022d09712b [        2048:      4194304 ]
   2: 516e7cba-6ecf-11d6-8ff8-00022d09712b [     4196352:    484200448 ]
Use (W)hole disk MBR, whole disk (G)PT or (E)dit? [whole] W
Setting OpenBSD MBR partition to whole sd0...done.

The auto-allocated layout for sd0 is
#                size           offset  fstype [fsize bsize   cpg]
  a:             1.0G               64  4.2BSD   2048 16384     1 # /
  b:             4.2G          2097216    swap
  c:           232.9G                0  unused
  d:             4.0G         10941664  4.2BSD   2048 16384     1 # /tmp
  e:            11.9G         19330272  4.2BSD   2048 16384     1 # /var
  f:             2.0G         44359200  4.2BSD   2048 16384     1 # /usr
  g:             1.0G         48553504  4.2BSD   2048 16384     1 # /usr/X11R6
  h:            20.0G         50650656  4.2BSD   2048 16384     1 # /usr/local
  i:             2.0G         92593696  4.2BSD   2048 16384     1 # /usr/src
  j:             6.0G         96788000  4.2BSD   2048 16384     1 # /usr/obj
  k:           180.7G        109370944  4.2BSD   4096 32768     1 # /home

Use (A)uto layout, (E)dit auto layout, or create (C)ustom layout? [a]
```

```
a               20g         /
b               4.3g        swap
d               6g          /tmp
e               30g         /var
f               20g         /usr
g               30g         /opt
h               7.5g        /home
```

```
OpenBSD area: 64-488392065; size: 488392001; free: 304190805
#                size           offset  fstype [fsize bsize   cpg]
  a:         20980800               64  4.2BSD   2048 16384     1 # /
  b:          9028556         20980864    swap
  c:        488397168                0  unused
  d:         12594912         30009440  4.2BSD   2048 16384     1 # /tmp
  e:         62926624         42604352  4.2BSD   2048 16384     1 # /var
  f:         20980896        105530976  4.2BSD   2048 16384     1 # /usr
  g:         41945696        126511872  4.2BSD   2048 16384     1 # /opt
  h:         15743712        168457568  4.2BSD   2048 16384     1 # /home

sd0> q #  quit & save changes
Write new label?: [y]
(...)
Which disk do you wish to initialize? (or 'done') [done]
```

Install software from the internet
```
Let's install the sets!
Location of sets? (disk http or 'done') [http]
HTTP proxy URL? (e.g. 'http://proxy:8080', or 'none') [none]
(Unable to get list from ftp.openbsd.org, but that is OK)
HTTP Server? (hostname or 'done') http://ftp.nluug.nl
Server directory? [pub/OpenBSD/6.5/amd64]

Select sets by entering a set name, a file name pattern or 'all'. De-select
sets by prepending a '-', e.g.: '-game*'. Selected sets are labelled '[X]'.
    [X] bsd           [X] base65.tgz    [X] game65.tgz    [X] xfont65.tgz
    [X] bsd.mp        [X] comp65.tgz    [X] xbase65.tgz   [X] xserv65.tgz
    [X] bsd.rd        [X] man65.tgz     [X] xshare65.tgz
Set name(s)? (or 'abort' or 'done') [done] -game*
    [X] bsd           [X] base65.tgz    [ ] game65.tgz    [X] xfont65.tgz
    [X] bsd.mp        [X] comp65.tgz    [X] xbase65.tgz   [X] xserv65.tgz
    [X] bsd.rd        [X] man65.tgz     [X] xshare65.tgz
Set name(s)? (or 'abort' or 'done') [done] -x*
    [X] bsd           [X] base65.tgz    [ ] game65.tgz    [ ] xfont65.tgz
    [X] bsd.mp        [X] comp65.tgz    [ ] xbase65.tgz   [ ] xserv65.tgz
    [X] bsd.rd        [X] man65.tgz     [ ] xshare65.tgz
Set name(s)? (or 'abort' or 'done') [done] -comp*
    [X] bsd           [X] base65.tgz    [ ] game65.tgz    [ ] xfont65.tgz
    [X] bsd.mp        [ ] comp65.tgz    [ ] xbase65.tgz   [ ] xserv65.tgz
    [X] bsd.rd        [X] man65.tgz     [ ] xshare65.tgz
Set name(s)? (or 'abort' or 'done') [done]

Get/Verify SHA256.sig   100% |**************************|  2141       00:00
Signature Verified
Get/Verify bsd          100% |**************************| 15163 KB    00:10
Get/Verify bsd.mp       100% |**************************| 15248 KB    00:10
Get/Verify bsd.rd       100% |**************************|  9984 KB    00:06
Get/Verify base65.tgz   100% |**************************|   190 MB    02:11
Get/Verify man65.tgz    100% |**************************|  7385 KB    00:05
Installing bsd          100% |**************************| 15163 KB    00:00
Installing bsd.mp       100% |**************************| 15248 KB    00:00
Installing bsd.rd       100% |**************************|  9984 KB    00:00
Installing base65.tgz   100% |**************************|   190 MB    00:25
Extracting etc.tgz      100% |**************************|   260 KB    00:00
Installing man65.tgz    100% |**************************|  7385 KB    00:01
Location of sets? (disk http or 'done') [done]

What timezone are you in? ('?' for list) [Canada/Mountain] Europe/Amsterdam

Saving configuration files... done.
Making all device nodes... done.
Multiprocessor machine; using bsd.mp instead of bsd.
Relinking to create unique kernel...
Relinking to create unique kernel... done.

CONGRATULATIONS! Your OpenBSD install has been successfully completed!

When you login to your new system the first time, please read your mail
using the 'mail' command.

Exit to (S)hell, (H)alt or (R)eboot? [reboot]
```

Log in as root and install any [pending updates](https://man.openbsd.org/syspatch), unless already [following -current](https://www.openbsd.org/faq/current.html):

Show all available patches first:
```
$ syspatch -c

001_rip6cksum
002_srtp
003_mds
004_bgpd
005_libssl
```

Apply patches:
```
$ syspatch
```

Install any pending firmware updates:
```
$ fw_update
```

Edit /etc/doas.conf to allow the regular user to run privileged commands without a password:
```
permit nopass keepenv :wheel
permit nopass keepenv root
```
Install any needed software:

```
$ pkg_add vim zsh curl pftop vnstat
```
Log out as root when finished.


@Reint: 
```
em0:        192.168.2.1/24
em1:        192.168.240.1/24
```

See the Asciicast for more information on the installation
```
$ asciinema play --speed=2 ./asciicast/OpenBSD-install.cast
```

Connect via SSH
```
$ ssh -o PreferredAuthentications=password -o PubkeyAuthentication=no 192.168.10.180
```

## Install packages

```
vi /etc/installurl

http://ftp.nluug.nl/pub/OpenBSD
```
Install iperf3

```
pkg_add iperf3 screen htop
```

```
Ambiguous: choose package for screen
a	0: <None>
	1: screen-4.0.3p6
	2: screen-4.0.3p6-shm
Your choice: 1
```

## Send to speaker

```
$ echo "MLo2e#..c#.e..e#.." > /dev/speaker
```


## References

* [drduh/PC-Engines-APU-Router-Guide](https://github.com/drduh/PC-Engines-APU-Router-Guide)
    * Used updated BIOS v4.9.0.5
* [Github: OpenBSD on PC Engines APU2](https://github.com/elad/openbsd-apu2)
* [List of File Sets](https://www.openbsd.org/faq/faq4.html)
* [Youtube: Install OpenBSD 6.0 on a APU.2c4](https://www.youtube.com/watch?v=U1evihAk8DY)
* [OpenBSD as house alarm system Openbsd is not only good for network related projects](https://www.youtube.com/watch?v=M7yzABO8PyY) by Vincent Delft @FOSDEM 2018
* [Unable to boot on APU2C4](http://openbsd-archive.7691.n7.nabble.com/Unable-to-boot-on-APU2C4-td294499.html)
* [OpenBSD Manual Partitioning at Installation](https://dev.to/nabbisen/openbsd-manual-partitioning-at-installation-4cif)
* [Flashing the BIOS on the PC Engines APU4c4](https://github.com/lattera/articles/blob/master/hardware/apu/2019-02-05_flashing_bios/article.md)

### Possible errors
If you see errors like this:
```
cannot open hd0a:/etc/random.seed: No such file or directory
booting hd0a:/6.4/amd64/bsd.rd: 3511114+1500160+3892040+0+598016 [372715+111+441
072+293323]=0xa208a0
entry point at 0x100015
```
This means that the USB stick that you're using to install OpenBSD is not supported. I had issues with Sandisk Cruzer Fit USB drives. I've had success with the Samsung Bar Plus 32GB Titanium USB drive.


[OpenVPN and OpenBSD 6.0](https://lab.rickauer.com/post/2016/10/15/OpenVPN-and-OpenBSD-6.0)

[OpenBSD and PCEngine's APU](https://lab.rickauer.com/post/2016/08/15/OpenBSD-and-PCEngines-APU)

[Things I have set up at home](https://www.linkedin.com/pulse/things-i-have-set-up-home-joe-pierce-systems-network-administrator/)


[nstalling FreeBSD on PC Engines APU2](https://blog.zs64.net/2018/07/installing-freebsd-on-pc-engines-apu2/)

[openbsd-ports including openzwave](https://github.com/jcs/openbsd-ports/tree/master/comms/openzwave)

[OpenBSD FAQ - Installation Guide](https://www.openbsd.org/faq/faq4.html)


[Stephan Rickauer's Hacking Lab - OpenBSD](https://lab.rickauer.com/category/OpenBSD)

[WireGuard is an extremely simple yet fast and modern VPN](https://www.wireguard.com/)

[PCEngines - apu2-documentation](https://github.com/pcengines/apu2-documentation)
[PCEngines - BIOS](https://pcengines.github.io/)
[Github: OpenBSD Router Boilerplate](https://github.com/vedetta-com/vedetta)
[homebrew-openbsd-pcengines-router](https://github.com/martinbaillie/homebrew-openbsd-pcengines-router)
[Github - PC-Engines-APU-Router-Guide](https://github.com/drduh/PC-Engines-APU-Router-Guide/blob/master/README.md)
* [Flashing the BIOS on the PC Engines APU4c4 - NOTE: I have an APU2C4](https://github.com/lattera/articles/blob/master/hardware/apu/2019-02-05_flashing_bios/article.md)
* [Building a BSD home router (pt. 1): Hardware (PC Engines APU2)](https://eerielinux.wordpress.com/2017/05/30/building-a-bsd-home-router-pt-1-hardware-pc-engines-apu2/)

### Tuning

<a href="http://www.dslreports.com/speedtest/41009920">
<img src="http://www.dslreports.com/speedtest/41009920.png"></a>

