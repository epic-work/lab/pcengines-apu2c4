# Install FreeBSD 12.0 on `pcengines` box

## Download FreeBSD form mirror in The Netherlands

Go to the [FreeBSD list of mirrors](https://www.freebsd.org/doc/handbook/mirrors-ftp.html#mirrors-nl-ftp) and download the latest release of FreeBSD 12.0

Alternatively, you can download the latest version of FreeBSD from [TransIP's Mirror](http://mirror.transip.net/)


### Validate Checksums file using GPG (DIDN'T DO THIS)

Download FreeBSD public keyring
```
curl -L -O https://www.freebsd.org/doc/pgpkeyring.txt
```

Import FreeBSD public key
```
$ gpg --import OSSEC-ARCHIVE-KEY.asc
```

The checksums can be found here: https://www.freebsd.org/releases/12.0R/signatures.html
```
$ curl -L -O https://www.freebsd.org/releases/12.0R/CHECKSUM.SHA256-FreeBSD-12.0-RELEASE-amd64.asc
$ gpg --verify FreeBSD-12.0-RELEASE-amd64-disc1.iso.xz
$ gpg --verify FreeBSD-12.0-RELEASE-amd64-memstick.img.xz
```

I don't know which GPG key was used by the FreeBSD team. More info: https://serverfault.com/questions/896228/how-to-verify-a-file-using-an-asc-signature-file
https://www.freebsd.org/doc/handbook/pgpkeys.html

### Validate checksums
```
$ shasum -a 256 FreeBSD-12.0-RELEASE-amd64-memstick.img.xz
830d1ab5aa18e3d55f26fbc1f804a422509229f60ca1f90f8098139acbe7f353  FreeBSD-12.0-RELEASE-amd64-memstick.img.xz

$ shasum -a 256 FreeBSD-12.0-RELEASE-amd64-disc1.iso.xz
1d40015bea89d05b8bd13e2ed80c40b522a9ec1abd8e7c8b80954fb485fb99db  FreeBSD-12.0-RELEASE-amd64-disc1.iso.xz
```

### Extract the archive
```
xz -d FreeBSD-12.0-RELEASE-amd64-memstick.img.xz
xz -d FreeBSD-12.0-RELEASE-amd64-disc1.iso.xz
```

## Create bootable USB key in OSx

```
diskutil list

/dev/disk2 (external, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      GUID_partition_scheme                        *15.4 GB    disk2
   1:                        EFI EFI                     209.7 MB   disk2s1
   2:                 Apple_APFS                         15.2 GB    disk2s2
```

```
diskutil unmountDisk /dev/diskN
```
(replace N with the disk number from the last command; in the previous example, N would be 2)

```
$ dd bs=1m if=/Users/egbert/Downloads/FreeBSD-12.0-RELEASE-amd64-memstick.img of=/dev/rdiskN conv=sync
```

Now eject the USB drive
```
$ diskutil eject /dev/diskN
```
... and remove your flash media when the command completes

## Installation

During the boot, I escaped to loader prompt (3) and added:

```
set boot_multicons="YES"
set boot_serial="YES"
set comconsole_speed="115200"
set kern.cam.boot_delay="10000"
set console="comconsole,vidconsole"
boot
```

**NOTE: You have to be vary carefull when copy-pasting the lines above via `Minicom`. I noticed you can only copy-paste a few characters**

**Before finishing the installation, you should open a shell and edit `/boot/loader.conf`**
```
boot_multicons="YES"
boot_serial="YES"
comconsole_speed="115200"
console="comconsole,vidconsole"
amdtemp_load="YES"
```

Read CPU temperatures from the APU2/3
```
$ egbert@gatekeeper:~ % su
Password:

root@gatekeeper:/home/egbert # kldload amdtemp.ko
amdtemp0: <AMD CPU On-Die Thermal Sensors> on hostb5
root@gatekeeper:/home/egbert # sysctl dev.amdtemp
dev.amdtemp.0.core0.sensor0: 50.2C
dev.amdtemp.0.sensor_offset: 0
dev.amdtemp.0.%parent: hostb5
dev.amdtemp.0.%pnpinfo:
dev.amdtemp.0.%location:
dev.amdtemp.0.%driver: amdtemp
dev.amdtemp.0.%desc: AMD CPU On-Die Thermal Sensors
dev.amdtemp.%parent:
```

```
Fri Sep 28 03:17
FreeBSD/amd64 (gatekeeper) (ttyu0)

login: Sep 28 03:17:43 gatekeeper ntpd[53988]: error resolving pool 0.freebsd.pool.ntp.org: hostname nor servname provided, or not known (8)


FreeBSD/amd64 (gatekeeper) (ttyu0)

login: egbert
Password:
Last login: Fri Sep 28 03:13:06 on ttyu0
FreeBSD 12.0-RELEASE r341666 GENERIC

Welcome to FreeBSD!

Release Notes, Errata: https://www.FreeBSD.org/releases/
Security Advisories:   https://www.FreeBSD.org/security/
FreeBSD Handbook:      https://www.FreeBSD.org/handbook/
FreeBSD FAQ:           https://www.FreeBSD.org/faq/
Questions List: https://lists.FreeBSD.org/mailman/listinfo/freebsd-questions/
FreeBSD Forums:        https://forums.FreeBSD.org/

Documents installed with the system are in the /usr/local/share/doc/freebsd/
directory, or can be installed later with:  pkg install en-freebsd-doc
For other languages, replace "en" with a language code like de or fr.

Show the version of FreeBSD installed:  freebsd-version ; uname -a
Please include that output and any error messages when posting questions.
Introduction to manual pages:  man man
FreeBSD directory layout:      man hier

Edit /etc/motd to change this login announcement.
You can press Ctrl-D to quickly exit from a shell, or logout from a
login shell.
                -- Konstantinos Konstantinidis <kkonstan@duth.gr>
```

####  Configure network interfaces

`/etc/rc.conf`
```
clear_tmp_enable="YES"
sendmail_enable="NONE"
hostname="gatekeeper"
defaultrouter="94.168.12.145"
#defaultrouter="172.16.254.249"
sshd_enable="YES"
ntpd_enable="YES"
ntpd_sync_on_start="YES"
gateway_enable="YES"
powerd_enable="YES"
# Set dumpdev to "AUTO" to enable crash dumps, "NO" to disable
dumpdev="NO"
zfs_enable="YES"
# Configure first ethernet port igb0
# Accept internet connection on igb0
ifconfig_igb0="inet 94.168.12.146/29"
# Configure second ethernet port igb1
# Add VLAN 200 on igb1
#vlans_igb1="vlan200"
#create_args_vlan200="vlan 200"
#ifconfig_vlan200="inet 192.168.10.5/24"
# Configure third ethernet port igb2
ifconfig_igb1="inet 192.168.10.5/24"
ifconfig_igb2="inet 172.16.254.250/29"
#
# Enable PF firewall
pf_enable="YES"
pf_rules="/usr/local/etc/pf.conf"
pflog_enable="YES"
pflog_flags=""
pflog_logfile="/var/log/pflog"
# Enable vnstat
# vnstat_enable="YES"
```


```
ifconfig_vlan23="inet 10.23.1.1 netmask 255.255.255.240 vlan 23 vlandev em0"
```

#### Configure `ntpd` to sync the time on restart

Edit `/etc/rc.conf` and add the line `ntpd_sync_on_start="YES"`

This will sync the time of your device, even if the HW-clock has a big offset vs the NTP clocks
```
ntpd_enable="YES"
ntpd_sync_on_start="YES"
```

#### Update OS to latest patch level

```
$ freebsd-update fetch
src component not installed, skipped
Looking up update.FreeBSD.org mirrors... 3 mirrors found.
Fetching public key from update5.freebsd.org... done.
Fetching metadata signature for 11.1-RELEASE from update5.freebsd.org... done.
Fetching metadata index... done.
Fetching 2 metadata files... done.
Inspecting system... done.
Preparing to download files... done.
Fetching 327 patches.....10....20....30....40....50....60....70....80....90....100....110....120....130....140....150....160....170....180....190....200....210....220....230....240....250....260....270....280....290....300....310....320... done.
Applying patches... done.

The following files will be updated as part of updating to 11.1-RELEASE-p10:
/bin/freebsd-version
/bin/pgrep
/bin/pkill
/bin/ps
```

```
$ freebsd-update install
```

#### Install and configure `bash`

See: https://www.cyberciti.biz/faq/freebsd-bash-installation/

```
pkg install bash
```

```
$ pkg install bash-completion
```

NIET GEDAAN
Then you need to update /etc/fstab with the following command [Source](https://www.bartdekker.nl/index.php/operating-systems-os/freebsd/20-installing-the-bash-shell-on-freebsd):
```
echo fdesc    /dev/fd        fdescfs        rw    0    0 >> /etc/fstab
```
WEL GEDAAN
Paste this into `~/.bashrc` to enable bash-completion
```
[[ $PS1 && -f /usr/local/share/bash-completion/bash_completion.sh ]] && \
	. /usr/local/share/bash-completion/bash_completion.sh
```


### Install and configure sudo

Sudo is a software which is designed to allow a common user to execute commands with the security privileges of the superuser account. Sudo utility is not installed by default in FreeBSD

```
$ pkg install sudo
```

Open sudoers configuration file, located in `/usr/local/etc/` directory, for editing by executing visudo command.

```
$ visudo
```

Add the regular user to system group called `wheel` and uncomment the `wheel` group from `sudoers` file by removing the `#` sign at the beginning of the line.

```
%wheel ALL=(ALL) NOPASSWD: ALL
```

Now add your user to the `wheel` group
```
$ pw groupmod wheel -M egbert
```


#### Install and configure `zsh`

[Source](https://tech.aapjeisbaas.nl/setup-oh-my-zsh-on-freebsd.html)

Become root

```
su -
```

```
pkg install zsh curl git
```

now we can set the zsh as the default shell for our user
```
chsh -s zsh egbert
```

Close the ssh session and reopen it. You will be greated with a zsh welcome message, close this with "q"

Lets pull in oh-my-zsh
```
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```


#### Install VIM

```
pkg install vim-console
```

You can also create a alias using alias command:
```
alias vi='vim'
```
Add above alias to your shell start up file such as `~/.bashrc`;  `~/.cshrc` or `~/.zshrc`

```
echo "alias vi='vim'" >> ~/.zshrc
```

More info: https://www.cyberciti.biz/faq/howto-install-vim-text-editor-under-freebsd/

#### Install vnstat

```
$ pkg install vnstat
```

Enable `vnstat` on boot. Edit `/etc/rc.conf`

```
vnstat_enable="YES"
```

Edit `/usr/local/etc/vnstat.conf`
```
# vnStat 2.0 config file
##

# default interface
Interface "igb0"

(...)
```

Enable `vnstat`
```
$ sudo service vnstat enable
```

Start `vnstat`
```
$ sudo service vnstat start
```

Check the status
```
$ sudo service vnstat status
```


#### Harden SSH

```
$ sudo vi /etc/ssh/sshd_config
```

```
LogLevel VERBOSE
PermitRootLogin no
MaxAuthTries 6

PubkeyAuthentication yes

PasswordAuthentication no
PermitEmptyPasswords no

ChallengeResponseAuthentication no
```

**NOTE: make this consistent with other SSH hardening documentation**

Restart the SSH daemon
```
$ sudo /etc/rc.d/sshd restart
```

### Install `rsync`
```
$ pkg install rsync
```

### Install `drill` 

`drill` is a `dig` alternative on FreeBSD

```
$ pkg install drill
```

#### Recover from bad `/etc/rc.conf` file
```
$ mount -u 
$ zfs mount -a
$ vi /etc/rc.conf
$ reboot
```

#### Install DHCP server

```
$ pkg install isc-dhcp44-server


Message from isc-dhcp44-server-4.4.1_3:

****  To setup dhcpd, please edit /usr/local/etc/dhcpd.conf.

****  This port installs the dhcp daemon, but doesn't invoke dhcpd by default.
      If you want to invoke dhcpd at startup, add these lines to /etc/rc.conf:

            dhcpd_enable="YES"                          # dhcpd enabled?
            dhcpd_flags="-q"                            # command option(s)
            dhcpd_conf="/usr/local/etc/dhcpd.conf"      # configuration file
            dhcpd_ifaces=""                             # ethernet interface(s)
            dhcpd_withumask="022"                       # file creation mask

****  If compiled with paranoia support (the default), the following rc.conf
      options are also supported:

            dhcpd_chuser_enable="YES"           # runs w/o privileges?
            dhcpd_withuser="dhcpd"              # user name to run as
            dhcpd_withgroup="dhcpd"             # group name to run as
            dhcpd_chroot_enable="YES"           # runs chrooted?
            dhcpd_devfs_enable="YES"            # use devfs if available?
            dhcpd_rootdir="/var/db/dhcpd"       # directory to run in
            dhcpd_includedir="<some_dir>"       # directory with config-
                                                  files to include

****  WARNING: never edit the chrooted or jailed dhcpd.conf file but
      /usr/local/etc/dhcpd.conf instead which is always copied where
      needed upon startup.
```


Create file `/usr/local/etc/dhcpd.conf.vlan200`
```
subnet 192.168.10.0 netmask 255.255.255.0 {
  default-lease-time         600;
  max-lease-time             72400;
  option routers             192.168.10.5;
  option broadcast-address   192.168.10.255;
  option subnet-mask         255.255.255.0;
  option domain-search       "epiclab.local";
  option domain-name-servers 62.179.104.196,213.46.228.196;
  pool {
    range 192.168.10.51 192.168.10.250;

    host ESXi {
        hardware ethernet 0C:C4:7A:9C:9F:12;
        fixed-address 192.168.10.2;
    }
    host UbiquitiACAPpro {
        hardware ethernet F0:9F:C2:7C:99:44;
        fixed-address 192.168.10.3;
    }
    host MikrotikSwitch {
        hardware ethernet CC:2D:E0:00:58:B6;
        fixed-address 192.168.10.6;
    }
    host storage01 {
        hardware ethernet AC:1F:6B:83:21:14;
        fixed-address 192.168.10.4;
    }
  }
}
```

Create file `/usr/local/etc/dhcpd.conf.vlan300`
```
subnet 192.168.128.0 netmask 255.255.255.0 {
  default-lease-time         600;
  max-lease-time             72400;
  option broadcast-address   192.168.128.255;
  option subnet-mask         255.255.255.0;
  option domain-search       "ipmi.local";
  pool {
    range 192.168.128.51 192.168.128.250;

    host HomelabIPMI {
        hardware ethernet 0C:C4:7A:9C:9D:8E;
        fixed-address 192.168.128.2;
    }
    host StorageIPMI {
        hardware ethernet AC:1F:6B:86:06:55;
        fixed-address 192.168.128.3;
    }
  }
}
```

Create af ile /usr/local/etc/dhcpd.conf
```
include "/usr/local/etc/dhcpd.conf.vlan200";
include "/usr/local/etc/dhcpd.conf.vlan300";
```

Edit `/etc/rc.conf`
```
dhcpd_enable="YES"
dhcpd_conf="/usr/local/etc/dhcpd.conf"
dhcpd_ifaces="vlan200 vlan300"
```

```
$ service isc-dhcpd status
```


#### Set default route

```
$ route add default 192.168.1.254
```

Or edit `/etc/rc.conf` and do 
```
$ service routing restart
```



#### Install `pf` firewall

Edit `/etc/rc.conf`

```
pf_enable="YES"
pf_rules="/usr/local/etc/pf.rules"
pflog_enable="YES"  
pflog_flags=""  
pflog_logfile="/var/log/pflog"  
```


Configure the firewall using the `/etc/pf.conf` file.

```
vi /usr/local/etc/pf.conf
```

This is the contents of the file
```
set skip on lo0
pass out quick all

tcp_services = "{ ssh, http, https }"

block in all
pass in quick inet proto icmp all
pass in quick inet6 proto icmp6 all
pass in proto tcp from any to any port $tcp_services
```

To enable the firewall:
```
echo 'pf_enable="YES"' >> /etc/rc.conf
service pf start
```

Reload `pf` rules
```
$ pfctl -f /usr/local/etc/pf.conf
```

More notes on `pf`

* [How To Set Up PF Firewall on FreeBSD to Protect a Web Server](https://www.cyberciti.biz/faq/how-to-set-up-a-firewall-with-pf-on-freebsd-to-protect-a-web-server/)
* [Edge routing with HardenedBSD](https://emeraldonion.org/edge-routing-with-hardenedbsd/)
* [Howto secure FreeBSD](https://fleximus.org/howto/secure-freebsd)
* [A Beginner's Guide To Firewalling with pf](http://srobb.net/pf.html)
* [PF firewall on FreeBSD for allowing SSH and OpenVPN Traffic](https://serverfault.com/questions/867854/pf-firewall-on-freebsd-for-allowing-ssh-and-openvpn-traffic)


#### ElastiFlow

* [ElastiFlow installation](https://github.com/robcowart/elastiflow/blob/master/INSTALL.md)




## FreeBSD forwarding performance

* [EMERALD ONION - Edge routing with HardenedBSD](https://emeraldonion.org/edge-routing-with-hardenedbsd/)
* [Introduction to HardenedBSD World](https://vermaden.wordpress.com/2018/04/06/introduction-to-hardenedbsd-world/)
* [FreeBSD forwarding Performance](https://bsdrp.net/documentation/technical_docs/performance)
* [Recipe for building a 10Mpps FreeBSD based router](http://blog.cochard.me/2015/09/receipt-for-building-10mpps-freebsd.html)
* [netbenches](https://github.com/ocochard/netbenches/blob/master/AMD_G-T40E_2Cores_RTL8111E/forwarding-pf-ipfw/results/fbsd11-routing.r287531/README.md)

* [HardenedBSD](https://hardenedbsd.org/)
* [Shawn Webb Tells You All About HardenedBSD Project – Interview by Luca Ferrari](https://bsdmag.org/shawn_webb/)

* [Building a BSD home router (pt. 6): pfSense vs. OPNsense](https://eerielinux.wordpress.com/2017/06/25/building-a-bsd-home-router-pt-6-pfsense-vs-opnsense/)

