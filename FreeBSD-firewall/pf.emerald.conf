#########
#
# Emerald Onion 
#
#########
#
###### Variables and data used throughout this document:
#
### Dynamic tables
table <hardened_updates> persist
table <hardened_updates6> persist
table <ssh_permitted_hosts> { } persist
table <tor_instances> const { 23.129.64.0/24, !23.129.64.1 } persist
table <tor_instances6> const { 2620:18c::0/36, !2620:18c::1 } persist

# SIX BGP sources
h4_six_bgp="{ 206.81.80.0/23 }"
h6_six_bgp="{ 2001:504:16::0/64 }"
h4_six_bgp_nonat="206.81.80.0/23"
h6_six_bgp_nonat="2001:504:16::0/64"

# Private addresses
h_tor_relay="23.129.64.10"

### Ports of note
p_ssh = "22"
p_dns = "53"
p_hbsd_update = "{ 80 443 }"
p_tor = "{ 80 443 }"
p_ntp = "123"
p_bgp = "179"
p_internal_permitted = "{" $p_ntp $p_dns "}"

### Interfaces - Outside
i_six = "ix1"
# functionally equivalent to eo_egress interface group - need to collapse rules using this alias
i_wan = "{" $i_six "}"
a_wan = "23.129.64.1" 
a6_wan = "2620:18c::1"

### Interfaces - Inside
i_lan = "{" ix0 "}"

### Permissible ICMP
icmp_types = "{ echoreq unreach }"
icmp6_types = "{ toobig echoreq neighbrsol neighbradv }"
i_wan_icmp6_types="{ echoreq listendone routeradv neighbrsol neighbradv redir }"

#
###### Options section
# Do not process loopback traffic
set skip on lo0
set limit { states 200000, frags 40000, src-nodes 50000 }

# Scrub (normalize) packets to ensure consistent rule assignment
scrub in all

#
###### NAT/RDR
#
### NAT Rules
# Anything sent out by the host to a BGP peer should use the native address. Everything else should NAT to the advertised address
nat on $i_six inet from ($i_six) to ! $h4_six_bgp_nonat -> $a_wan
nat on $i_six inet6 from ($i_six) to ! $h6_six_bgp_nonat -> $a6_wan

#
### Port Forwarding (RDR)
# Remember your filter rules below! These rules only translate incoming traffic, the filter rules decide what is allowed....
# These two rules should be collapsed to use eo_egress...
rdr on $i_six inet proto tcp from any to $a_wan port $p_ssh -> $h_tor_relay port $p_ssh
rdr on $i_six inet proto tcp from any to $a_wan port = 2222 -> $a_wan port $p_ssh

#
###### Filter
#
# Strict default policy
block all

# Block spurious emissions
block quick on $i_six inet6 from 2620:18c::1/128 to any

# Permit DNS requests from anything - interface group eo_egress includes $i_wow and $i_six
pass out quick on eo_egress inet proto { tcp udp } to any port = $p_dns keep state 
pass out quick on eo_egress inet6 proto { tcp udp } to any port = $p_dns keep state

# Permit openBGPd
pass out quick on $i_six inet proto tcp from ($i_six) to $h4_six_bgp port = $p_bgp keep state
pass out quick on $i_six inet6 proto tcp from ($i_six) to $h6_six_bgp port = $p_bgp keep state
pass in quick on $i_six inet proto tcp from $h4_six_bgp to ($i_six) port = $p_bgp keep state
pass in quick on $i_six inet6 proto tcp from $h6_six_bgp to ($i_six) port = $p_bgp keep state

# Permit SSH from authorized...
pass in quick on eo_egress inet proto tcp from <ssh_permitted_hosts> to $h_tor_relay port $p_ssh keep state
pass in quick on eo_egress inet proto tcp from <ssh_permitted_hosts> to $a_wan port $p_ssh keep state
pass out quick on ix0 inet proto tcp from <ssh_permitted_hosts> to $h_tor_relay port $p_ssh keep state
# ...and block from anyone else
block in quick on eo_egress inet proto tcp from any to any port $p_ssh 
block in quick on eo_egress inet6 proto tcp from any to any port $p_ssh

# Permit anything to the Tor servers
pass in quick on eo_egress inet from any to <tor_instances> 
pass out quick on ix0 inet from any to <tor_instances> 
pass in quick on ix0 inet from <tor_instances> to !(ix0) 
pass out quick on eo_egress inet from <tor_instances> to any 
pass in quick on eo_egress inet6 from any to <tor_instances6> 
pass out quick on ix0 inet6 from any to <tor_instances6> 
pass in quick on ix0 inet6 from <tor_instances6> to !(ix0) 
pass out quick on eo_egress inet6 from <tor_instances6> to any

# Allow Tor traffic to transit the router but do not allow it towards our management network
#pass in quick on ix0 from ix0:network to !(ix0) 
#pass out quick on eo_egress from ix0:network to any 
## IPv6 address hasn't been configured on this interface yet
#pass in quick on ix0 inet6 from ix0:network to !(ix0) 
#pass out quick on $i_wan inet6 from ix0:network to any

## Permitted egress traffic
# Hardened BSD Updates
# Script provided to update <hardened_updates> and <hardened_updates6> tables on-demand although the addresses should be relatively stable
# Any host is allowed to these servers on 80/443
pass out quick on eo_egress inet proto tcp from $a_wan to <hardened_updates> port $p_hbsd_update keep state
#pass out quick on eo_egress inet6 proto tcp from $a_wan to <hardened_updates6> port $p_hbsd_update keep state

# Permit ICMP facilities
pass out quick on eo_egress inet proto icmp all icmp-type $icmp_types keep state
pass out quick on eo_egress inet6 proto ipv6-icmp all icmp6-type $icmp6_types keep state
pass in quick on eo_egress inet proto icmp all icmp-type $icmp_types keep state
pass in quick on eo_egress inet6 proto ipv6-icmp icmp6-type $icmp6_types keep state
pass in quick on $i_six inet6 proto ipv6-icmp from any to { ($i_six) ff02::1/16 } icmp6-type $i_wan_icmp6_types keep state
# And for internal services...
pass quick on $i_lan inet proto icmp all icmp-type $icmp_types keep state
pass quick on $i_lan inet6 proto ipv6-icmp all icmp6-type $icmp6_types keep state

# Permit DNS and NTP requests to us from our internal interfaces
pass in quick on $i_lan inet proto { tcp udp } from ix0:network to (ix0) port $p_internal_permitted keep state

# Protect the router from internal users - block everything else (not DNS or NTP requests)
block in quick on ix0 from ix0:network to (ix0)