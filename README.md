# Serial connection

* Use a null-modem cable
* Do not use gender changers
* The default baud rate for apu boards is 115200,8n1

### Install minicom

```
$ brew install minicom
```

### Install the USB to serial drivers

The PCEngines usbcom1a uses the Silicon Labs CP2104 controller for which you can grab the driver [here](https://www.silabs.com/products/mcu/pages/usbtouartbridgevcpdrivers.aspx) (Mac/Linux/Windows)

2018 update: to get this driver working on Sierra I had to install the legacy version of v5 twice. The double install gets the devices created and kernel driver loaded. No response from SiLabs on a fix at the time of writing this.


### Determine the USB to Serial port
```
$ cd /dev
$ ls | grep tty

tty
tty.Bluetooth-Incoming-Port
tty.SLAB_USBtoUART
tty.SOC
(..)
```

### Configure `minicom` for first use
```
$ mincom -s

            ┌─────[configuration]──────┐
            │ Filenames and paths      │
            │ File transfer protocols  │
            │ Serial port setup        │
            │ Modem and dialing        │
            │ Screen and keyboard      │
            │ Save setup as dfl        │
            │ Save setup as..          │
            │ Exit                     │
            │ Exit from Minicom        │
            └──────────────────────────┘
```

Select `Serial port setup`

Type `A` to edit the path to the serial device. Enter the serial device name you've found in the step above. In my case this would be `/dev/tty.SLAB_USBtoUART`.
```
    ┌───────────────────────────────────────────────────────────────────────┐
    │ A -    Serial Device      : /dev/tty.SLAB_USBtoUART                   │
    │ B - Lockfile Location     : /usr/local/Cellar/minicom/2.7.1/var       │
    │ C -   Callin Program      :                                           │
    │ D -  Callout Program      :                                           │
    │ E -    Bps/Par/Bits       : 115200 8N1                                │
    │ F - Hardware Flow Control : No                                        │
    │ G - Software Flow Control : No                                        │
    │                                                                       │
    │    Change which setting?                                              │
    └───────────────────────────────────────────────────────────────────────┘
            │ Screen and keyboard      │
            │ Save setup as dfl        │
            │ Save setup as..          │
            │ Exit                     │
            │ Exit from Minicom        │
            └──────────────────────────┘
```

Now, type `E` to set the `Bps/Pa/Bits`
```
    ┌─────────────────┌─────────[Comm Parameters]──────────┐────────────────┐
    │ A -    Serial De│                                    │                │
    │ B - Lockfile Loc│     Current: 115200 8N1            │2.7.1/var       │
    │ C -   Callin Pro│ Speed            Parity      Data  │                │
    │ D -  Callout Pro│ A: <next>        L: None     S: 5  │                │
    │ E -    Bps/Par/B│ B: <prev>        M: Even     T: 6  │                │
    │ F - Hardware Flo│ C:   9600        N: Odd      U: 7  │                │
    │ G - Software Flo│ D:  38400        O: Mark     V: 8  │                │
    │                 │ E: 115200        P: Space          │                │
    │    Change which │                                    │                │
    └─────────────────│ Stopbits                           │────────────────┘
            │ Screen a│ W: 1             Q: 8-N-1          │
            │ Save set│ X: 2             R: 7-E-1          │
            │ Save set│                                    │
            │ Exit    │                                    │
            │ Exit fro│ Choice, or <Enter> to exit?        │
            └─────────└────────────────────────────────────┘
```

The default baud rate of the `PC Engines APU2C4` and `PC Engines APU2D4` is 115200 Bps. I use `115200 8N1` (8 data bits, no parity, 1 stop bit) which works just fine. 

Pres `ESC` two times and choose `Save setup as dfl`. Now minicom is ready to use

### Use Screen as your serial console

```
$ screen /dev/tty.SLAB_USBtoUART
```

## First boot of the PCEngines apu2c4 board 

Directly after booting the `apu2c4` board, pres `F10` to open the boot menu. 

```
coreboot build 20170228
4080 MB ECC DRAM

SeaBIOS (version rel-1.10.0.1)

Press F10 key now for boot menu

Select boot device:

1. ata0-0: Samsung SSD 860 EVO mSATA 250GB ATA-11 Hard-Disk (2
2. Payload [memtest]
3. Payload [setup]

Booting from CBFS...

### PC Engines apu2 setup v4.0.4 ###
Boot order - type letter to move device to top.

  a USB 1 / USB 2 SS and HS
  b SDCARD
  c mSATA
  d SATA
  e iPXE (disabled)


  r Restore boot order defaults
  n Network/PXE boot - Currently Disabled
  t Serial console - Currently Enabled
  l Serial console redirection - Currently Enabled
  u USB boot - Currently Enabled
  o UART C - Currently Disabled
  p UART D - Currently Disabled
  x Exit setup without save
  s Save configuration and exit
```

My board runs on coreboot v4.0.4. According to the website of https://www.coreboot.org/downloads.html, the latest version of `coreboot` is version 4.8.1

## BIOS update

Many people advise to upgrade the BIOS to the latest `mainline` version. However, at the time of writing, this version has a known issue: [CPU clock (is) stuck at low frequency](https://github.com/pcengines/coreboot/issues/196). 

I decided to use the `legacy` BIOS for the moment. The latest available `legacy` BIOS available is version `4.0.21`, however DO NOT USE version `4.0.21` ([more info](http://www.pcengines.info/forums/?page=post&id=4C472C95-E846-42BF-BC41-43D1C54DFBEA&fid=6D8DBBA4-9D40-4C87-B471-80CB5D9BD945)). Use version `4.0.20` instead

To flash this version of the BIOS on the board, you can use [PCEngine's TinyCoreLinux](http://pcengines.ch/howto.htm#TinyCoreLinux)

#### Download
```
$ mkdir bios-upgrade-pcengines
$ cd bios-upgrade-pcengines
$ curl -O -L http://pcengines.ch/file/apu2-tinycore6.4.img.gz
$ curl -O -L http://pcengines.ch/file/apu2_v4.0.20.rom.tar.gz
```

#### Unzip
```
$ gunzip apu2-tinycore6.4.img.gz
$ tar -xzvf apu2_v4.0.20.rom.tar.gz
```

#### Verify
```
$ md5 apu2-tinycore6.4.img # 48b8e0f21792648889aa99bf8156fed7

$ shasum -a 256 apu2-tinycore6.4.img # f5a20eeb01dfea438836e48cb15a18c5780194fed6bf21564fc7c894a1ac06d7

$ md5 apu2_v4.0.20.rom # 93dbfda609fd42ed14178139b295173a 
$ cat apu2_v4.0.20.rom.md5 # 93dbfda609fd42ed14178139b295173a
```

```
$ diskutil list

/dev/disk2 (external, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:     FDisk_partition_scheme                        *15.8 GB    disk2
   1:             Windows_FAT_32 NO NAME                 15.8 GB    disk2s1
```

```
$ diskutil unmountDisk /dev/diskN
```
(replace N with the disk number from the last command; in the previous example, N would be 2)

Now create a bootable USB drive with TinyCoreLinux on it
```
$ sudo dd if=apu2-tinycore6.4.img of=/dev/rdisk2 bs=1m conv=sync
```

Copy the `apu2_v4.0.20.rom` file to your bootable `TinyCoreLinux` USB drive
```
$ cp apu2_v4.0.20.rom /Volumes/SYSLINUX/. 
$ cp apu2_v4.0.20.rom.md5 /Volumes/SYSLINUX/. 
```

```
$ diskutil unmountDisk /dev/disk2
Unmount of all volumes on disk2 was successful
```

## Bios upgrade -- update Oct 13 2019

To flash this version of the BIOS on the board, you can use [PCEngine's TinyCoreLinux](http://pcengines.ch/howto.htm#TinyCoreLinux)

#### Download
```
$ mkdir bios-upgrade-pcengines
$ cd bios-upgrade-pcengines
$ curl -O -L http://pcengines.ch/file/apu2-tinycore6.4.img.gz
$ curl -O -L https://3mdeb.com/open-source-firmware/pcengines/apu2/apu2_v4.10.0.2.rom
$ curl -O -L https://3mdeb.com/open-source-firmware/pcengines/apu2/apu2_v4.10.0.2.SHA256
```

#### Unzip
```
$ gunzip apu2-tinycore6.4.img.gz
```

#### Verify
```
$ md5 apu2-tinycore6.4.img # 48b8e0f21792648889aa99bf8156fed7
$ shasum -a 256 apu2-tinycore6.4.img # f5a20eeb01dfea438836e48cb15a18c5780194fed6bf21564fc7c894a1ac06d7

$ shasum -a 256 apu2_v4.10.0.2.rom # 689923f0e9e1dc1cc07152eae89bc472faa69a7f5e1dae2edffff47a2696c51b
$ cat apu2_v4.10.0.2.SHA256        # 689923f0e9e1dc1cc07152eae89bc472faa69a7f5e1dae2edffff47a2696c51b  apu2_v4.10.0.2.rom
```

```
$ diskutil list

/dev/disk2 (external, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:     FDisk_partition_scheme                        *15.8 GB    disk2
   1:             Windows_FAT_32 NO NAME                 15.8 GB    disk2s1
```

```
$ diskutil unmountDisk /dev/diskN
```
(replace N with the disk number from the last command; in the previous example, N would be 2)

Now create a bootable USB drive with TinyCoreLinux on it
```
$ sudo dd if=apu2-tinycore6.4.img of=/dev/rdisk2 bs=1m conv=sync
```

Copy the `apu2_v4.10.0.2.rom` file to your bootable `TinyCoreLinux` USB drive
```
$ cp apu2_v4.10.0.2.rom /Volumes/SYSLINUX/. 
```

```
$ diskutil unmountDisk /dev/disk2
Unmount of all volumes on disk2 was successful
```

### Flash the BIOS

With the USB in a slot, power cycle the router. Either fire in an F10 again, or wait and the boot order should kick in and launch TinyCore Linux from the USB.

Startup of TinyCore 6.4 looks like:
```sh
TinyCore 6.4 www.tinycorelinux.com

login[616]: root login on 'ttyS0'
searching for home directory ...
FAT partition not mounted yet, retrying since 0 sscsi 4:0:0:0: Direct-Access     Samsung  Flash Drive      1100 P6
sd 4:0:0:0: Attached scsi generic sg1 type 0
sd 4:0:0:0: [sdb] 62656641 512-byte logical blocks: (32.0 GB/29.8 GiB)
sd 4:0:0:0: [sdb] Write Protect is off
sd 4:0:0:0: [sdb] Write cache: enabled, read cache: enabled, doesn't support DPO or FUA
FAT partition not mounted yet, retrying since 2 s sdb: sdb1
sd 4:0:0:0: [sdb] Attached SCSI removable disk
random: nonblocking pool is initialized
FAT partition not mounted yet, retrying since 14 sSorry, FAT partition not found.
-sh: /root/takemehome.sh: line 22: fail_beep: not found


-sh: /root/takemehome.sh: ./autostart.sh: not found
root@box:~#
```

This should land you in a rootshell with `/media/SYSLINUX` the mounted USB. Proceed to flash the ROM:
```
$ flashrom -p internal -w /media/SYSLINUX/apu2_v4.10.0.2.rom

flashrom v0.9.8-r1888 on Linux 3.16.6-tinycore (i686)
flashrom is free software, get the source code at http://www.flashrom.org

Calibrating delay loop... OK.
coreboot table found at 0xdffae000.
Found chipset "AMD FCH".
Enabling flash write... OK.
Found Winbond flash chip "W25Q64.V" (8192 kB, SPI) mapped at physical address 0xff800000.
This coreboot image (PC Engines:apu2) does not appear to
be correct for the detected mainboard (PC Engines:PCEngines apu2).
Aborting. You can override this with -p internal:boardmismatch=force
```

```
$ flashrom -p internal:boardmismatch=force -w /media/SYSLINUX/apu2_v4.10.0.2.rom

flashrom v0.9.8-r1888 on Linux 3.16.6-tinycore (i686)
flashrom is free software, get the source code at http://www.flashrom.org

Calibrating delay loop... OK.
coreboot table found at 0xdffae000.
Found chipset "AMD FCH".
Enabling flash write... OK.
Found Winbond flash chip "W25Q64.V" (8192 kB, SPI) mapped at physical address 0xff800000.
This coreboot image (PC Engines:apu2) does not appear to
be correct for the detected mainboard (PC Engines:PCEngines apu2).
Aborting. You can override this with -p internal:boardmismatch=force.
root@box:~# flashrom -p internal:boardmismatch=force -w /media/SYSLINUX/apu2_v4.
10.0.2.rom
flashrom v0.9.8-r1888 on Linux 3.16.6-tinycore (i686)
flashrom is free software, get the source code at http://www.flashrom.org

Calibrating delay loop... delay loop is unreliable, trying to continue OK.
coreboot table found at 0xdffae000.
Found chipset "AMD FCH".
Enabling flash write... OK.
Found Winbond flash chip "W25Q64.V" (8192 kB, SPI) mapped at physical address 0xff800000.
This coreboot image (PC Engines:apu2) does not appear to
be correct for the detected mainboard (PC Engines:PCEngines apu2).
Proceeding anyway because user forced us to.
Reading old flash chip contents... done.
Erasing and writing flash chip... Erase/write done.
Verifying flash... VERIFIED.
```

Reboot after you see the final `Verifying flash... VERIFIED.`
```
$ reboot
```


If all went well, you should see:
```
PC Engines apu2
coreboot build 20190810
BIOS version v4.10.0.2
4080 MB ECC DRAM

SeaBIOS (version rel-1.12.1.3-0-g300e8b7)

Press F10 key now for boot menu
```

## Install packages on FreeBSD

```
$ pkg add iperf3
```

## Update FreeBSD

```
$ freebsd-update fetch
$ freebsd-update install
```

## Asciicast

Watch
```
asciinema rec asciicast/OpenBSD-install.cast
asciinema rec pcengines/asciicast/asciicast/OpenBSD-speedtest-htop.cast
asciinema play /path/to/file.cast
asciinema play --speed=2 ~/Development/EPIC/pcengines/asciicast/OpenBSD-speedtest.cast
asciinema play --speed=2 ~/Development/EPIC/pcengines/asciicast/OpenBSD-speedtest-htop.cast

rm asciicast/OpenBSD-install.cast
```


```
$ ssh -o PubkeyAuthentication=no 192.168.10.11
```

create new screen
```
Ctrl+a c
```


Split screen
```
Ctrl + a
:split
```

Switch to active screen
```
Ctrl +a 0-9
```
