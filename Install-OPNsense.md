## Download 

https://opnsense.org/download/

Check if the checksums matches
```
$ shasum -a 256 OPNsense-18.7-OpenSSL-serial-amd64.img.bz2
a4556080532d22e9ab296e2c6e163b3d65d5fe54a642253e1c01a22721afa850  OPNsense-18.7-OpenSSL-serial-amd64.img.bz2
```

Unzip the image
```
$ bunzip2 OPNsense-18.7-OpenSSL-serial-amd64.img.bz2
```

Check where your USB drive was mounted
```
$ diskutil list

/dev/disk0 (internal):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      GUID_partition_scheme                         500.3 GB   disk0
   1:                        EFI EFI                     314.6 MB   disk0s1
   2:                 Apple_APFS Container disk1         500.0 GB   disk0s2

/dev/disk1 (synthesized):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      APFS Container Scheme -                      +500.0 GB   disk1
                                 Physical Store disk0s2
   1:                APFS Volume Macintosh HD            468.0 GB   disk1s1
   2:                APFS Volume Preboot                 51.2 MB    disk1s2
   3:                APFS Volume Recovery                512.8 MB   disk1s3
   4:                APFS Volume VM                      3.2 GB     disk1s4

/dev/disk2 (external, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:     FDisk_partition_scheme                        *32.1 GB    disk2
   1:                       0xEF                         491.5 KB   disk2s1
   2:                    OpenBSD                         377.0 MB   disk2s4
```

Wipe the USB drive (optional)

```
$ sudo dd if=/dev/zero of=/dev/disk2 bs=1 count=1024
```

Write image to USB drive

```
$ sudo dd bs=1m conv=sync if=~/Downloads/OPNsense-18.7-OpenSSL-serial-amd64.img of=/dev/rdisk2
```

## First boot of the PCEngines apu2c4 board 

Directly after booting the `apu2c4` / `apu2d4` board, pres `F10` to open the boot menu. 

